<?php
$DBUSER = 'moji';
$DBPASSWORD = 'moji';
$DBHOST = 'localhost';
$DATABASE = 'db542256881';
//	$SAMTABLENAME = 'sams';
$BATTERYTABLENAME = 'battery';
$LOGTABLENAME = 'logs';

//HOWTO
if (!isset($_GET['output'])) {
    echo "Usage: [URL_to_this_php]?[parameter1_name]=[parameter1_value]&[parameter2_name]=[parameter2_value][&...]" . "<br>\n";
    echo "<br>\n";
    echo "Parameters with default values are optional." . "<br>\n";
    echo "<br>\n";
    echo "Parameter:" . "<br>\n";
    echo "&emsp; text" . "<br>\n";
    echo "&emsp;&emsp; - any random letters or combinations to calculate features for" . "<br>\n";
    echo "&emsp;&emsp; - multiple values separated with comma possible" . "<br>\n";
    echo "&emsp;&emsp; - default: top ten bigraphs in Deutsch" . "<br>\n";
    echo "<br>\n";
    echo "&emsp; feature (not implemented)" . "<br>\n";
    echo "&emsp;&emsp; - [all | ...]" . "<br>\n";
    echo "&emsp;&emsp; - name abbreviation for the feature (see 2018-04-15_feature_reduction.xls (without graph) -> 1d2d_avg; 1d2d_std; ...)" . "<br>\n";
    echo "&emsp;&emsp; - multiple values separated with comma possible" . "<br>\n";
    echo "&emsp;&emsp; - default: all" . "<br>\n";

    echo "<br>\n";
    echo "&emsp; energy_score" . "<br>\n";
    echo "&emsp;&emsp; - [1 to 7]" . "<br>\n";
    echo "&emsp;&emsp; - max energy" . "<br>\n";
    echo "&emsp;&emsp; - min,max when two values separated with comma" . "<br>\n";
    echo "&emsp;&emsp; - default: 1,5" . "<br>\n";
    echo "<br>\n";
    echo "&emsp; energyfiltered" . "<br>\n";
    echo "&emsp;&emsp; - [true | false]" . "<br>\n";
    echo "&emsp;&emsp; - only sams with values within the given range will be output" . "<br>\n";
    echo "&emsp;&emsp; - use with caution!" . "<br>\n";
    echo "&emsp;&emsp; - default: false" . "<br>\n";
    echo "<br>\n";
    echo "&emsp; fixed" . "<br>\n";
    echo "&emsp;&emsp; - [true | false]" . "<br>\n";
    echo "&emsp;&emsp; - include log_type fixed_log" . "<br>\n";
    echo "&emsp;&emsp; - default: true" . "<br>\n";
    echo "<br>\n";
    echo "&emsp; output" . "<br>\n";
    echo "&emsp;&emsp; - [arff | json | jsontimes | phparray | phparraytimes | dump]" . "<br>\n";
    echo "&emsp;&emsp; - output format" . "<br>\n";
    echo "&emsp;&emsp; - jsontimes | phparraytimes: won't calculate any features and only return all necessary timecodes to do so" . "<br>\n";
    echo "<br>\n";
    exit(0);
}

//yeah ..
set_time_limit(0);
ini_set('display_errors', '1');
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

//DATABASE CONNECT
$connect = mysqli_connect($DBHOST, $DBUSER, $DBPASSWORD, $DATABASE);
if (!$connect) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
if (!mysqli_set_charset($connect, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($connect));
    exit();
}

//DUMP
if ($_GET['output'] === 'dump') {
    $FILE = $DATABASE . '_' . date('ymdHis') . '.sql.gz';

    header('Content-Type: application/x-gzip');
    header('Content-Disposition: attachment; filename="' . $FILE . '"');

    $CMD = 'mysqldump -u $DBUSER --password=$DBPASSWORD --extended-insert=FALSE --skip-extended-insert $DATABASE | gzip --best';
    passthru($CMD);

    exit(0);
}

//COMBINATIONS
if (isset($_GET['text'])) {
    $combinations = explode(",", $_GET['text']);
} else {
    $combinations = array('en', "er", "ch", "de", "ei", "ie", "in", "te", "ge",
    "un","ich","ein","der","sch","die","und","che","cht","den","gen");
}

//FEATURES
if (!isset($_GET['feature'])) {
    $features = 'all';
} else {
    $features = explode(",", $_GET['feature']);
}

//energy score
if (!isset($_GET['level'])) {
    $energy_score_min = 1;
    $energy_score_max = 7;
} else {
    $energy_score = explode(",", $_GET['level'], 2);
    if (count($energy_score) === 1) {
        $energy_score_min = 1;
        $energy_score_max = $energy_score[0];
    } else {
        $energy_score_min = $energy_score[0];
        $energy_score_max = $energy_score[1];
    }
}


//we'll get one instance at a time, to not have a huge array with all of the SQL data ...
//and since we don't have a proper ID, we'll use the timestamps

//ARRAY energy
$energies = array();
$result = mysqli_query($connect, 'SELECT * FROM ' . $BATTERYTABLENAME);
while ($row = mysqli_fetch_array($result)) {
    array_push($energies, array('secret' => $row['secret'],
        'scode' => $row['scode'],
        'timestamp' => $row['timestamp'],
        'level' => $row['level'],
        'user_id' => $row['user_id']));
}
mysqli_free_result($result);
unset($row);
unset($result);

$legend_printed = false;
$json_output = array();
//let's run through it ...
foreach ($energies as $energy_index => &$energy) {
    $energy['logs'] = array();

    //finding last sam timestamp from current person
    $reverse_sliced_array = array_reverse(array_slice($energies, 0, $energy_index, false), false);
    $reverse_sliced_array_index = array_search($energy['scode'], array_column($reverse_sliced_array, 'scode'));

    if ($reverse_sliced_array_index !== false) {
        $last_energy = $reverse_sliced_array[$reverse_sliced_array_index]['timestamp'];
    } else {
        $last_energy = '';
    }

    unset($reverse_sliced_array);
    unset($reverse_sliced_array_index);

    $result = mysqli_query($connect, 'SELECT * FROM ' . $LOGTABLENAME . ' WHERE scode="' . $energy['scode'] . '"');
    while ($row = mysqli_fetch_array($result)) {
        //for readability we'll make log an associative array
        $log = array();
        $log['scode'] = $row['scode'];
        $log['secret'] = $row['secret'];
        $log['browser_ver'] = $row['browser_ver'];
        $log['log_type'] = $row['log_type'];
        $log['log_text'] = $row['log_text'];
        $log['log_data_cut'] = 0;
        $log['log_data'] = $row['log_data'];

        //before we add it, we'll check if it's a dupe of an already exisiting log, or if it should be filtered
        $skip_log = false;
        foreach ($energy['logs'] as $energy_log) {
            if ($log['log_data'] == $energy_log['log_data']) {
                $skip_log = true;
            }
        }
        if (preg_match('/.*?"time":(\d*).*/', $log['log_data'], $log_time)) {
            if ($log_time[1] <= strtotime($last_energy) . '000' || $log_time[1] > $energy['timestamp'] . '000') {
                $skip_log = true;
            }
        } else {
            $skip_log = true;
        }
        if ($log['log_type'] === 'fixed_log' && isset($_GET['fixed']) && ($_GET['fixed'] === 'false')) {
            $skip_log = false;
        }

        //adding this log to this sam
        if (!$skip_log) {
            array_push($energy['logs'], $log);
        }
    }
    mysqli_free_result($result);
    unset($row);
    unset($result);
    unset($skip_log);
    unset($log);

    //checking if this sam needs to be filtered out
    $skip_energy = false;
    if (isset($_GET['energyfiltered']) && $_GET['energyfiltered'] === 'true') {
        if ($energy['energy_score'] < $energy_score_min || $energy['energy_score'] > $energy_score_max) {
            $skip_energy = true;
        }
    }
    /*	foreach($sam['logs'] as $sam_log) {
            if(strlen($sam_log['log_data']) <= 10) {
                $skip_sam = true;
            }
        }
    */
    if (count($energy['logs']) < 1) {
        $skip_energy = true;
    }

    if (!$skip_energy) {
        unset($skip_energy);
        //FIXING LOG_DATA

        foreach ($energy['logs'] as &$energy_log) {
            $old_data = $energy_log['log_data'];
            $decode_result = json_decode($old_data, true);

            //fixing the end of the cut data
            if (json_last_error() !== JSON_ERROR_NONE) {
                unset($decode_result);
                $energy_log['log_data_cut'] = 1;
                $brokendata = substr($old_data, 0, strrpos($old_data, '},')) . '}]';
                $decode_result = json_decode($brokendata, true);
                unset($brokendata);
            }
            $energy_log['log_data'] = $decode_result;
            unset($decode_result);
            unset($old_data);

            //let's fix one keystroke being registered / firing multiple times (almost exclusively shift and enter)
            //also deleting events without corresponding down or up event
            //yes, yes, I know, that also deletes some special character bullcrap like this: ^^ .. but we don't care about that anyway
            $down_events = array();
            foreach ($energy_log['log_data'] as $index => $current_event) {
                if (!isset($current_event['key'])) {
                    if (isset($current_event['which']) && $current_event['which'] > 0) {
                        $current_event['key'] = chr($current_event['which']);
                        $energy_log['log_data'][$index]['key'] = chr($current_event['which']);
                    } elseif (isset($current_event['keycode']) && $current_event['keycode'] > 0) {
                        $current_event['key'] = chr($current_event['keycode']);
                        $energy_log['log_data'][$index]['key'] = chr($current_event['keycode']);
                    } elseif (isset($current_event['charcode']) && $current_event['charcode'] > 0) {
                        $current_event['key'] = chr($current_event['charcode']);
                        $energy_log['log_data'][$index]['key'] = chr($current_event['charcode']);
                    }
                }
                if (isset($current_event['key']) && isset($current_event['etype']) && isset($current_event['time'])) {
                    $result = array_search(strtolower($current_event['key']), array_column($down_events, 'key'));
                    if ($result !== false) {
                        if ($current_event['etype'] === 'down') {
                            if ($index - $down_events[$result]['index'] < 2) {
                                //down shortly after other down -> deleting current one
                                unset($energy_log['log_data'][$index]);
                            } else {
                                //down further away -> deleting old one and adding new one
                                unset($energy_log['log_data'][$down_events[$result]['index']]);
                                array_splice($down_events, $result, 1);
                                $new_event = array('key' => strtolower($current_event['key']), 'index' => $index);
                                array_push($down_events, $new_event);
                                unset($new_event);
                            }
                        } else { //etype === up
                            array_splice($down_events, $result, 1);
                        }
                    } else { //result === false
                        if ($current_event['etype'] === 'down') {
                            $new_event = array('key' => strtolower($current_event['key']), 'index' => $index);
                            array_push($down_events, $new_event);
                            unset($new_event);
                        } else {
                            unset($energy_log['log_data'][$index]);
                        }
                    }
                    unset($result);
                } else {
                    unset($energy_log['log_data'][$index]);
                }
            }

            //since we're through with the log, all leftover down events without up events have to go
            foreach ($down_events as $down_event) {
                unset($energy_log['log_data'][$down_event['index']]);
            }

            unset($down_events);

            //fixing indexes:
            $energy_log['log_data'] = array_values($energy_log['log_data']);
        }

        //CREATING OUTPUT
        if (count($energy['logs']) > 0) {

            //print_r($sam);

            //SESSION TIME
            $sessiontime = 0;
            foreach ($energy['logs'] as $current_log) {
                if (count($current_log['log_data']) > 0) {
                    $sessiontime += $current_log['log_data'][count($current_log['log_data']) - 1]['time'] - $current_log['log_data'][0]['time'];
                }
            }

            //KEYSTROKES
            $keystrokes = 0;
            foreach ($energy['logs'] as $current_log) {
                $keystrokes += (count($current_log['log_data']) / 2);
            }

            //MISTAKES
            $mistakes = 0;
            foreach ($energy['logs'] as $current_log) {
                if (count($current_log['log_data']) > 0) {
                    foreach ($current_log['log_data'] as $event) {
                        if ($event['key'] === 'ArrowLeft' || $event['key'] === 'ArrowRight'
                            || $event['key'] === 'ArrowUp' || $event['key'] === 'ArrowDown'
                            || $event['key'] === 'Backspace' || $event['key'] === 'Delete') {
                            $mistakes += 0.5; //every keystroke has 2 events
                        }
                    }
                }
            }

            //SAM_STRESS
            //		$sam_stress = 1;
            //			if($sam['sam_valence'] < $sam_valence_min || $sam['sam_valence'] > $sam_valence_max) {
//					$sam_stress = 0;/
//				} elseif($sam['sam_arousal'] < $sam_arousal_min || $sam['sam_arousal'] > $sam_arousal_max) {/
//					$sam_stress = 0;
//				} elseif($sam['sam_dominance'] < $sam_dominance_min || $sam['sam_dominance'] > $sam_dominance_max) {
//					$sam_stress = 0;
//				}

            //LOG_CUT
            $log_cut = 0;
            foreach ($energy['logs'] as $current_log) {
                if ($current_log['log_data_cut'] === 1) {
                    $log_cut = 1;
                }
            }

            //COMBINATION SEARCH
            //going through every combination
            $combinations_out = array();
            foreach ($combinations as $combinations_index => $combination) {
                $combination_events = array();
                $letters = str_split($combination);
                $index_down = array_fill(0, count($letters), array('index' => 'false', 'time' => 0));
                $index_up = array_fill(0, count($letters), array('index' => 'false', 'time' => 0));
                //going through every log
                foreach ($energy['logs'] as $energy_log_index => $current_log) {
                    //going through every event
                    $combination_done = false;
                    foreach ($current_log['log_data'] as $energy_log_data_index => $energy_log_data) {
                        //going through every letter of the combination
                        $event_done = false;
                        foreach ($letters as $letter_index => $letter) {
                            if (!$event_done) {
                                if ($index_down[$letter_index]['index'] === 'false') {
                                    //first combination letter down
                                    if ($energy_log_data['etype'] === 'down' && strtolower($energy_log_data['key']) === strtolower($letter) && $letter_index === 0 && $index_down[0]['index'] === 'false') {
                                        $index_down[$letter_index]['index'] = $energy_log_data_index;
                                        $index_down[$letter_index]['time'] = $energy_log_data['time'];
                                        $event_done = true;
                                    } //other combination letter down
                                    elseif ($energy_log_data['etype'] === 'down' && strtolower($energy_log_data['key']) === strtolower($letter) && $letter_index > 0 && $index_down[$letter_index - 1]['index'] !== 'false') {
                                        $index_down[$letter_index]['index'] = $energy_log_data_index;
                                        $index_down[$letter_index]['time'] = $energy_log_data['time'];
                                        $event_done = true;
                                    }
                                }
                                if ($index_up[$letter_index]['index'] === 'false') {
                                    //letter up
                                    if (($energy_log_data['etype'] === 'up' || $energy_log_data['etype'] === 'pressed') && strtolower($energy_log_data['key']) === strtolower($letter) && $index_down[0]['index'] !== 'false' && $index_down[$letter_index]['index'] !== 'false') {
                                        $index_up[$letter_index]['index'] = $energy_log_data_index;
                                        $index_up[$letter_index]['time'] = $energy_log_data['time'];
                                        $event_done = true;
                                        $last_up = true;
                                        foreach ($index_up as $index_up2) {
                                            if ($index_up2['index'] === 'false') {
                                                $last_up = false;
                                            }
                                        }
                                        if ($last_up) {
                                            $combination_done = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (!$event_done) {
                            //key down after last letter down
                            if ($energy_log_data['etype'] === 'down' && $index_down[count($letters) - 1]['index'] !== 'false') {
                                $event_done = true;
                            } //shift or control
                            elseif ((strtolower($energy_log_data['key']) === 'shift' && $energy_log_data['etype'] === 'down') || strtolower($energy_log_data['key']) === 'control') {
                                $event_done = true;
                            } //down event breaking the combination
                            elseif ($energy_log_data['etype'] === 'down') {
                                $index_down = array_fill(0, count($letters), array('index' => 'false', 'time' => 0));
                                $index_up = array_fill(0, count($letters), array('index' => 'false', 'time' => 0));
                                $event_done = true;
                            }
                        }
                        if ($combination_done) {
                            $combination_event = array($index_down, $index_up);
                            array_push($combination_events, $combination_event);
                            $combination_done = false;
                            $index_down = array_fill(0, count($letters), array('index' => 'false', 'time' => 0));
                            $index_up = array_fill(0, count($letters), array('index' => 'false', 'time' => 0));
                        }
                    }
                }

                //FEATURE CALCULATION
                unset($dwell_time, $latency, $flight, $duration, $key_events);
                $dwell_time = array_fill(0, count($letters), array('avg' => 0, 'std' => 0));
                $latency = array_fill(0, count($letters) - 1, array('avg' => 0, 'std' => 0));
                $flight = array_fill(0, count($letters) - 1, array('avg' => 0, 'std' => 0));
                $duration = array('avg' => 0, 'std' => 0);
                $key_events = array('avg' => 0, 'std' => 0);

                //no point in calculating anything, if it doesn't exist or we only want times
                if (count($combination_events) > 0 && $_GET['output'] !== 'jsontime' && $_GET['output'] !== 'phparraytime') {
                    foreach ($letters as $letter_index => $letter) {
                        //AVG
                        foreach ($combination_events as $combination_current) {
                            $dwell_time[$letter_index]['avg'] += $combination_current[1][$letter_index]['time'] - $combination_current[0][$letter_index]['time'];
                            if ($letter_index < count($letters) - 1) { //no need for those calculations if it's the last letter
                                $latency[$letter_index]['avg'] += $combination_current[0][$letter_index + 1]['time'] - $combination_current[0][$letter_index]['time'];
                                $flight[$letter_index]['avg'] += $combination_current[0][$letter_index + 1]['time'] - $combination_current[1][$letter_index]['time'];
                            } else { //this needs to be calculated only for the last one
                                $duration['avg'] += $combination_current[1][$letter_index]['time'] - $combination_current[0][0]['time'];
                                $key_events['avg'] += $combination_current[1][$letter_index]['index'] - $combination_current[0][0]['index'] + 1;
                            }
                        }
                        $dwell_time[$letter_index]['avg'] /= count($combination_events);
                        if ($letter_index < count($letters) - 1) { //no need for those calculations if it's the last letter
                            $latency[$letter_index]['avg'] /= count($combination_events);
                            $flight[$letter_index]['avg'] /= count($combination_events);
                        } else { //this needs to be calculated only for the last one
                            $duration['avg'] /= count($combination_events);
                            $key_events['avg'] /= count($combination_events);
                        }

                        //STD
                        foreach ($combination_events as $combination_current) {
                            $dwell_time[$letter_index]['std'] += pow(($combination_current[1][$letter_index]['time'] - $combination_current[0][$letter_index]['time']) - $dwell_time[$letter_index]['avg'], 2);
                            if ($letter_index < count($letters) - 1) { //no need for those calculations if it's the last letter
                                $latency[$letter_index]['std'] += pow(($combination_current[0][$letter_index + 1]['time'] - $combination_current[0][$letter_index]['time']) - $latency[$letter_index]['avg'], 2);
                                $flight[$letter_index]['std'] += pow(($combination_current[0][$letter_index + 1]['time'] - $combination_current[1][$letter_index]['time']) - $flight[$letter_index]['avg'], 2);
                            } else { //this needs to be calculated only for the last one
                                $duration['std'] += pow(($combination_current[1][$letter_index]['time'] - $combination_current[0][0]['time']) - $duration['avg'], 2);
                                $key_events['std'] += pow(($combination_current[1][$letter_index]['index'] - $combination_current[0][0]['index']) + 1 - $key_events['avg'], 2);
                            }
                        }
                        $dwell_time[$letter_index]['std'] = sqrt($dwell_time[$letter_index]['std']);
                        if ($letter_index < count($letters) - 1) { //no need for those calculations if it's the last letter
                            $latency[$letter_index]['std'] = sqrt($latency[$letter_index]['std']);
                            $flight[$letter_index]['std'] = sqrt($flight[$letter_index]['std']);
                        } else { //this needs to be calculated only for the last one
                            $duration['std'] = sqrt($duration['std']);
                            $key_events['std'] = sqrt($key_events['std']);
                        }
                    }
                    $combinations_out[$combinations_index] = array($combination, $dwell_time, $latency, $flight, $duration, $key_events);
                }
                if ($_GET['output'] === 'jsontime' || $_GET['output'] === 'phparraytime') {
                    $combinations_out[$combination] = $combination_events;
                }
            }


            if ($_GET['output'] === 'json' || $_GET['output'] === 'phparray') {
                $json_energy = array();
                $json_energy += array('user_id' => $energy['user_id']);
                $json_energy += array('sam_id' => $energy['timestamp']);
                $json_energy += array('ST' => $sessiontime);
                $json_energy += array('NumKSt' => $keystrokes);
                if ($keystrokes !== 0) {
                    $json_energy += array('TimeKSt' => $sessiontime / $keystrokes);
                } else {
                    $json_energy += array('TimeKSt' => 0);
                }
                $json_energy += array('NumMist' => $mistakes);
                $json_energy += array('level' => intval($energy['level']));

                //$json_sam += array('SAM_Stress' => $sam_stress);
                $json_energy += array('LOG_Cut' => $log_cut);

                if (count($combinations_out) > 0) {
                    $json_combinations = array();
                    foreach ($combinations_out as $combinations_index => $combination) {
                        $json_features = array();
                        foreach ($combination[1] as $index => $dwell_time) {
                            $json_features += array($combination[0] . "_dwt" . ($index + 1) . "_avg" => $dwell_time['avg']);
                            $json_features += array($combination[0] . "_dwt" . ($index + 1) . "_std" => $dwell_time['std']);
                        }
                        foreach ($combination[2] as $index => $latency) {
                            $json_features += array($combination[0] . "_" . ($index + 1) . "d" . ($index + 2) . "d_avg" => $latency['avg']);
                            $json_features += array($combination[0] . "_" . ($index + 1) . "d" . ($index + 2) . "d_std" => $latency['std']);
                        }
                        foreach ($combination[2] as $index => $flight) {
                            $json_features += array($combination[0] . "_" . ($index + 1) . "u" . ($index + 2) . "d_avg" => $flight['avg']);
                            $json_features += array($combination[0] . "_" . ($index + 1) . "u" . ($index + 2) . "d_std" => $flight['std']);
                        }
                        $json_features += array($combination[0] . "_dur_avg" => $combination[4]['avg']);
                        $json_features += array($combination[0] . "_dur_std" => $combination[4]['std']);
                        $json_features += array($combination[0] . "_numEv_avg" => $combination[5]['avg']);
                        $json_features += array($combination[0] . "_numEv_std" => $combination[5]['std']);

                        $json_combinations += array($combination[0] => $json_features);
                    }
                    $json_energy += array('features' => $json_combinations);
                }

                $json_output += array($json_energy['sam_id'] => $json_energy);


            }


        }
    }
    unset($energy['logs']);
    //echo memory_get_usage() . "\n";
}
if ($_GET['output'] === 'json' || $_GET['output'] === 'jsontime') {

    $fh = fopen("moji.energy.json", 'w')
    or die("Error opening output file");
    fwrite($fh, json_encode($json_output, JSON_UNESCAPED_UNICODE));
    fclose($fh);

} elseif ($_GET['output'] === 'phparray' || $_GET['output'] === 'phparraytime') {
    print_r($json_output);
}
?>