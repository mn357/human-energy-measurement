import numpy as np
import pandas as pd
from sklearn import  svm
import matplotlib.pyplot as plt
import seaborn as sb
from numpy.random import seed
from numpy.random import randn
from numpy import mean
from sklearn.preprocessing import LabelEncoder
from numpy import std
import json

#df = pd.read_json("data/moji.en.cs",orient='index')
#print (df["features"])
#exit()
#print (df.describe())
#corr = df.corr(method="pearson")
from sklearn.impute import SimpleImputer
import seaborn as sns
import json

def cleandataset(dataset,cleaned_dataset):
    df = pd.read_csv(dataset)
    df[:] = df[:].replace(np.nan,0)
    df[:] = df[:].replace("",0)
    cols = df.columns.str.replace("|" ,"")
    cols = cols.str.replace("__","")
    cols = cols.str.replace("featuresen","")
    cols = cols.str.replace("featureser","")
    cols = cols.str.replace("featuresch","")
    cols = cols.str.replace("featuresde","")
    cols = cols.str.replace("featuresei","")
    cols = cols.str.replace("featuresie","")
    cols = cols.str.replace("featuresin","")
    cols = cols.str.replace("featureste","")
    cols = cols.str.replace("featuresge","")
    cols = cols.str.replace("featuresun","")

    cols = cols.str.replace("featuresich","")
    cols = cols.str.replace("featuresein","")
    cols = cols.str.replace("featuresder","")
    cols = cols.str.replace("featuressch","")
    cols = cols.str.replace("featuresdie","")
    cols = cols.str.replace("featuresund","")
    cols = cols.str.replace("featuresche","")
    cols = cols.str.replace("featurescht","")
    cols = cols.str.replace("featuresden","")
    cols = cols.str.replace("featuresgen","")
    cols = cols.str.lower()
    df.columns = cols
    df.to_csv(cleaned_dataset)
import  pandas as pd
from numpy import percentile
import scipy.stats
#cleandataset("data/moji.en.csv","data/cleaned_energy.csv")
from sklearn.pipeline import Pipeline
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
#from sklearn.preprocessing import StandardScaler,Normalizer
df = pd.read_csv("data/cleaned_energy.csv")
X,y = df.values[:,10:], df.values[:,8]
X = X.astype('float32')
y = (y.astype('int'))
from sklearn.preprocessing import Normalizer

#va = pd.to_datetime(df['sam_id'])

#vv= df.groupby(va.dt.strftime("'%H-%M")).count()

data = df.values[:,4]

percentile
# identify outliers
#outliers = [x for x in data if (x < lower) | (x > upper)]
var= df.values[:, 4].astype('float32')
q25, q75 = percentile(data, 25), percentile(data, 75)
iqr = q75 - q25
print('Percentiles: 25th=%.3f, 75th=%.3f, IQR=%.3f' % (q25, q75, iqr))
# calculate the outlier cutoff
cut_off = iqr * 1 #k
lower, upper = q25 - cut_off, q75 + cut_off
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

va = pd.to_datetime(df['sam_id'])
#vv= df.groupby(va.dt.date).mean() #energy level accourding to each day,
vv= df.groupby(va.map(lambda  t : t.hour)).mean()
print(data.shape)



#vas = Normalizer().fit_transform(vas)
#vas = pd.DataFrame(vas,columns=['level',"numkst"])

#print (tabulate(pd.DataFrame(vas).corr(), headers='keys', tablefmt='psql'))
from sklearn.datasets import make_regression
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
from sklearn.preprocessing import StandardScaler
#X,y = df.values[:,10:], df.values[:,8]
#X = X.astype('float32')
#X  = Normalizer().fit_transform(X)
#standard= StandardScaler()
#X  = standard.fit_transform(X)
#fs = SelectKBest(score_func=f_regression, k=5)
#new_feature_set=  fs.fit_transform(X, y)
# Get columns to keep and create new dataframe with those only
#cols = fs.get_support(indices=True)
#features_df_new = df.columns[cols]
#print()
def heatMap(values, columns):
    f, ax = plt.subplots(figsize=(55, 55))
    from sklearn.preprocessing import Normalizer
    normalizeddf_train = Normalizer().fit_transform(values)
    corr = pd.DataFrame(normalizeddf_train).corr(method='pearson')  # data should be normilized
    # hm = sns.heatmap(corr, cmap="coolwarm", annot=False, linewidths=.5, xticklabels=columns, yticklabels = columns)
    # f.subplots_adjust(top=0.93)
    # t= f.suptitle('Keystroke Data Analyse', fontsize=14)
    # this can be printed for viewing in tabular manner
    # print(tabulate(corr, headers='keys', tablefmt='psql'))
    # print (corr[0])
    return corr[0]


#t = heatMap(df.values[:, 8:], df.columns[8:])

#vv = t.sort_values(ascending=False);
#co = df.columns [8:]
#[print (co[index]) for index in vv.index ]
#va = pd.to_datetime(df['sam_id'])
#vv= df.groupby(va.dt.date).mean()

#print(vv)
##standard = StandardScaler()
#normalizer = Normalizer()
#df  = pd.DataFrame (standard.fit_transform(df.values[:,5:]),columns=df.columns[5:])

#print(df.values[:,8])
#df.to_csv("data/standardized_energy.csv")
from sklearn.preprocessing import StandardScaler,Normalizer

#print ( df.describe()["level"])
#from sklearn.ensemble import RandomForestRegressor
#svm = RandomForestRegressor()
#pipeline = Pipeline(steps=[  ( "s", svm)])
# evaluate the pipeline
#cv = KFold(n_splits=10)
#n_scores = cross_val_score(pipeline, X, y,  scoring='neg_mean_squared_error', cv=cv, n_jobs=-1)
# report pipeline performance
#print('Mse: %.3f (%.3f)' % (mean(n_scores), std(n_scores)))
#print(df.values[:,8])

#df.boxplot();
#df.to_csv("cleaned.csv")







from sklearn.decomposition import PCA
import seaborn as sns
import numpy as np
import pandas as pd
from tabulate import tabulate
import seaborn as sns; sns.set (font_scale=1.1)
from sklearn.svm import OneClassSVM
from sklearn.covariance import EllipticEnvelope
from sklearn.ensemble import IsolationForest
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import PowerTransformer
from sklearn.preprocessing import FunctionTransformer
from sklearn.preprocessing import StandardScaler,Normalizer
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
from keras.wrappers.scikit_learn import KerasRegressor
from statistics import mean, median, mode, stdev
from keras.models import Sequential
from keras.layers import Conv1D, MaxPooling1D, Dense, Flatten,Dropout,Activation , BatchNormalization
from sklearn.ensemble import RandomForestRegressor
from keras.regularizers import l2,l1
from sklearn.impute import SimpleImputer
from numpy import percentile

X, y = data[:, 1:], data[:, 0]

X = X.astype('float32')
# pca = PCA(n_components=15) #determine number of COMPONENTS
# X = StandardScaler().fit_transform(X)
# X = pca.fit_transform(X)


# X  = Normalizer().fit_transform(X)
# X  = standard.fit_transform(X)
cross_val_score




